;; Copyright (C) 2017 Marc Nieper-Wißkirchen

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;> Make syntax transformers from syntax-rules.

(define-library (rapid macro-transformer)
  (export make-syntax-rules-transformer)
  (import (scheme base)
	  (rapid and-let)
	  (rapid receive)
	  (rapid list)
	  (rapid list-queue)
	  (rapid vector)
	  (rapid comparator)
       	  (rapid mapping)
	  (rapid syntax)
	  (rapid syntactic-environment))
  (include "macro-transformer.scm"))
