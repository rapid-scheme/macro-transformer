;; Copyright (C) 2017 Marc Nieper-Wißkirchen

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-library (rapid macro-transformer-test)
  (export run-tests)
  (import (scheme base)
          (rapid test)
	  (rapid syntax)
	  (rapid syntactic-environment)
          (rapid macro-transformer))
  (begin
    (define (ellipsis? identifier)
      (eq? identifier '...))
    (define (underscore? identifier)
      (eq? identifier '_))
    (define (literal? identifier)
      (and (memq identifier '(lit1 lit2 lit3)) #t))
    (define-syntax define-transformer
      (syntax-rules ()
	((define-transformer name rule* ...)
	 (define name (make-syntax-rules-transformer #f ellipsis? literal? underscore?
						     `(,(syntax rule*) ...))))))
    (define-syntax transform
      (syntax-rules ()
	((transform transformer source)
	 (syntax->datum (transformer (syntax source)) identifier->symbol))))
    
    (define (run-tests)
      (test-begin "Macro transformers")

      (test-group "Simple tests"
	(define-transformer transformer1
	  ((foo bar) (bla bar)))
	
	(test-equal '(bla baz) (transform transformer1 (foo baz))))
      
      (test-end))))
